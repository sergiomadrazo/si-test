package com.hotelbeds.supplierintegrations.hackertest.hackertest;

import com.hotelbeds.supplierintegrations.hackertest.tools.Tools;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ToolsTests {

    @Test
    public void twoHoursMore() throws ParseException {
        String time1 = "Thu, 21 Dec 2000 16:01:07 +0200";
        String time2 = "Thu, 21 Dec 2000 18:01:07 +0200";

        long minutes= Tools.minutesBetween(time1, time2);
        assertThat(minutes).isEqualTo(120L);
    }

    @Test
    public void oneMinuteMoreRoundedDown() throws ParseException {
        String time1 = "Thu, 21 Dec 2000 16:01:00 +0200";
        String time2 = "Thu, 21 Dec 2000 16:02:59 +0200";

        long minutes= Tools.minutesBetween(time1, time2);
        assertThat(minutes).isEqualTo(1L);
    }

    @Test
    public void towMinuteMoreRoundedDown() throws ParseException {
        String time1 = "Thu, 21 Dec 2000 16:01:00 +0200";
        String time2 = "Thu, 21 Dec 2000 16:03:00 +0200";

        long minutes= Tools.minutesBetween(time1, time2);
        assertThat(minutes).isEqualTo(2L);
    }

    @Test
    public void threeHoursMoreChangeTimeZone() throws ParseException {
        String time1 = "Thu, 21 Dec 2000 16:01:07 +0200";
        String time2 = "Thu, 21 Dec 2000 18:01:07 +0100";

        long minutes= Tools.minutesBetween(time1, time2);
        assertThat(minutes).isEqualTo(180L);
    }
}
