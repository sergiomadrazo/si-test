package com.hotelbeds.supplierintegrations.hackertest.hackertest;

import com.hotelbeds.supplierintegrations.hackertest.detector.HackerDetector;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HackertestDetectorTests {
    @Autowired
    private HackerDetector hackerDetector;

    @Test
    public void parseValidLine() {
        String res = hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_SUCCESS,Will.Smith");
        assertThat(res).isEqualTo(null);
    }

    @Test
    public void parseInvalidLine() {
        String res = hackerDetector.parseLine("80.238.9.179,133612947");
        assertThat(res).isEqualTo(null);
    }

    @Test
    public void parseEmptyLine() {
        String res = hackerDetector.parseLine("");
        assertThat(res).isEqualTo(null);
    }

    @Test
    public void parseNullLine() {
        String res = hackerDetector.parseLine(null);
        assertThat(res).isEqualTo(null);
    }

    @Test
    public void fiveFailureLoginAttempts() {
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_SUCCESS,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_SUCCESS,Will.Smith");
        String res = hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        assertThat(res).isEqualTo("80.238.9.179");
    }

    @Test
    public void fourFailureLoginAttempts() {
        hackerDetector.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612948,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612949,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,133612950,SIGNIN_FAILURE,Will.Smith");
        String res = hackerDetector.parseLine("80.238.9.179,133612951,SIGNIN_SUCCESS,Will.Smith");
        assertThat(res).isNull();
    }

    @Test
    public void fiveFailureLoginAttemptsInMoreThanFiveMinutes() {
        //17 February 2019 0:00:00
        hackerDetector.parseLine("80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith");
        hackerDetector.parseLine("80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith");
        //17 February 2019 0:05:01
        String res = hackerDetector.parseLine("80.238.9.179,1550379660,SIGNIN_FAILURE,Will.Smith");
        assertThat(res).isNull();
    }
}
