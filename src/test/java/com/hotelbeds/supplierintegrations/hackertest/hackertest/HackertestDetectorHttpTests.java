package com.hotelbeds.supplierintegrations.hackertest.hackertest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HackertestDetectorHttpTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void fiveFailureLoginAttempts() throws Exception {
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612948,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612949,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612950,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612951,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("80.238.9.179")));
    }

    @Test
    public void fourFailureLoginAttempts() throws Exception{
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612948,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612949,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612950,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,133612951,SIGNIN_SUCCESS,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
    }

    @Test
    public void fiveFailureLoginAttemptsInMoreThanFiveMinutes() throws Exception{
        //17 February 2019 0:00:00
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550361600,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("80.238.9.179")));
        //17 February 2019 0:05:01
        this.mockMvc.perform(get("/parseLine?line=80.238.9.179,1550379660,SIGNIN_FAILURE,Will.Smith\""))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("")));
    }

}
