package com.hotelbeds.supplierintegrations.hackertest.hackertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.hotelbeds.supplierintegrations")
public class HackertestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackertestApplication.class, args);
	}

}
