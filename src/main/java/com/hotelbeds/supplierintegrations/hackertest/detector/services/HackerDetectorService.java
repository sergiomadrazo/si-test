package com.hotelbeds.supplierintegrations.hackertest.detector.services;

import com.hotelbeds.supplierintegrations.hackertest.detector.LineCounter;
import com.hotelbeds.supplierintegrations.hackertest.detector.model.Line;
import com.hotelbeds.supplierintegrations.hackertest.detector.model.LineStack;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class HackerDetectorService {
    private final int LIMIT_TIME = 300; // 5 minutes
    private final int LIMIT_ATTEMPTS = 5;

    @Resource
    LineStack lineStack;

    public String checkAndSave(Line line){
        String result = null;

        LineCounter lineCounterFounded = lineStack.getStack().get(line.getIp());
        Long now = Long.parseLong(line.getDate());

        if(lineCounterFounded != null){
            // Ya lo habíamos encontrado
            if(lineCounterFounded.getLastLoginFailure() >= (now - LIMIT_TIME)){
                // Su último login ha sido hace menos de 5 minutos
                lineCounterFounded.setAttemps(lineCounterFounded.getAttemps()+1);
                lineCounterFounded.setLastLoginFailure(now);
                if (lineCounterFounded.getAttemps() >= LIMIT_ATTEMPTS){
                    // ip maliciosa.
                    result = line.getIp();
                }
            } else {
                // Hace más de 5 minutos. Comenzamos cuenta de nuevo
                lineCounterFounded.setAttemps(lineCounterFounded.getAttemps()+1);
                lineCounterFounded.setLastLoginFailure(now);
            }
        } else {
            // Es el primer login erróneo
            LineCounter lineCounter = new LineCounter();
            lineCounter.setLastLoginFailure(now);
            lineCounter.setAttemps(1);
            lineStack.getStack().put(line.getIp(), lineCounter);
        }
        return result;
    }

    public void checkAndDelete(Line line){
        LineCounter lineCounterFounded = lineStack.getStack().get(line.getIp());
        Long now = Long.parseLong(line.getDate());
        if(lineCounterFounded != null) {
            // Si la hemos encontrado, miramos si hace más de 5 minutos que hubo fallo
            if (lineCounterFounded.getLastLoginFailure() < (now - LIMIT_TIME)) {
                lineStack.getStack().remove(line.getIp());
            }
        }
    }
}
