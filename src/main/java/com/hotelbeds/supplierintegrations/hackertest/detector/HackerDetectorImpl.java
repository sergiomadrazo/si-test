package com.hotelbeds.supplierintegrations.hackertest.detector;

import com.hotelbeds.supplierintegrations.hackertest.detector.model.Line;
import com.hotelbeds.supplierintegrations.hackertest.detector.model.LineStack;
import com.hotelbeds.supplierintegrations.hackertest.detector.services.HackerDetectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class HackerDetectorImpl implements HackerDetector {
    @Autowired
    HackerDetectorService hackerDetectorService;

    @Override
    public String parseLine(String line) {
        // En caso de que el parámetro de entrada no sea correcto, devolverá null
        String res = null;

        if (line != null && !"".equals(line)){
            String[] splitted = line.split(",");
            if(splitted.length == 4){
                // Línea válida
                Line lineObject = createLine(splitted);
                if (lineObject.isFailLogin()){
                    // Guardamos el login erróneo
                    res = hackerDetectorService.checkAndSave(lineObject);
                } else {
                    // Probamos a borrarlo de la lista para ahorro de memmoria
                    hackerDetectorService.checkAndDelete(lineObject);
                }
            }
        }
        return res;
    }

    private Line createLine(String splittedLine[]){
        Line lineObject = new Line();
        lineObject.setIp(splittedLine[0]);
        lineObject.setDate(splittedLine[1]);
        lineObject.setAction(splittedLine[2]);
        lineObject.setUsername(splittedLine[3]);
        return lineObject;
    }
}
