package com.hotelbeds.supplierintegrations.hackertest.detector.model;

public class Line {
    private static String FAILURELOGIN = "SIGNIN_FAILURE";
    private String ip;
    private String date;
    private String action;
    private String username;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFailLogin(){
        if(FAILURELOGIN.equals(action)){
            return true;
        } else {
            return false;
        }
    }
}
