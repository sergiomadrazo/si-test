package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.util.Date;

public class LineCounter {
    private long lastLoginFailure;
    private Integer attemps;

    public long getLastLoginFailure() {
        return lastLoginFailure;
    }

    public void setLastLoginFailure(long lastLoginFailure) {
        this.lastLoginFailure = lastLoginFailure;
    }

    public Integer getAttemps() {
        return attemps;
    }

    public void setAttemps(Integer attemps) {
        this.attemps = attemps;
    }
}
