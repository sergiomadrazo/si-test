package com.hotelbeds.supplierintegrations.hackertest.detector.model;

import com.hotelbeds.supplierintegrations.hackertest.detector.LineCounter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class LineStack {
    private HashMap<String, LineCounter> stack = null;
    @Bean
    public HashMap<String, LineCounter> getStack(){
        if (stack == null) {
            stack = new HashMap<>();
        }
        return stack;
    }
}
