package com.hotelbeds.supplierintegrations.hackertest.controller;


import com.hotelbeds.supplierintegrations.hackertest.detector.HackerDetector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HackerDetectorController {
    @Autowired
    private HackerDetector hackerDetector;

    @RequestMapping("/parseLine")
    @ResponseBody
    public String parseLine(String line){
        return hackerDetector.parseLine(line);
    }

}
