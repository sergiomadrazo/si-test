package com.hotelbeds.supplierintegrations.hackertest.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Tools {
    private static final String RFCFORMAT = "EEE, dd MMM yyyy HH:mm:ss Z";

    public static long minutesBetween(String time1, String time2) throws ParseException {
        Date date1=Tools.rfctoDate(time1);
        Date date2=Tools.rfctoDate(time2);
        long difference = date2.getTime() - date1.getTime();
        return difference / 60000; // Milisegundos a minutos
    }

    private static Date rfctoDate(String rfcDate) throws ParseException {
        // RFC 2822 format (ie: Thu, 21 Dec 2000 16:01:07 +0200).
        SimpleDateFormat format = new SimpleDateFormat(RFCFORMAT, Locale.ENGLISH);
        return format.parse(rfcDate);
    }

}
